//
//  DetailsTableViewCell.swift
//  StatisticsGNB
//
//  Created by Eduard Preduca on 1/10/19.
//  Copyright © 2019 Eduard Preduca. All rights reserved.
//

import UIKit

class DetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var skuLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    
    @IBOutlet weak var eurAmountLabel: UILabel!
    @IBOutlet weak var eurCurrencyLabel: UILabel!
    
    @IBOutlet weak var eurConversionStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateWithTransaction(transaction: Transaction) {
        self.skuLabel.text = transaction.sku
        self.amountLabel.text = String(transaction.amount)
        currencyLabel.text = transaction.currency
        
        if transaction.currency != "EUR" {
            eurAmountLabel.isHidden = false
            eurCurrencyLabel.isHidden = false
            
            eurAmountLabel.text = String(transaction.eurAmout)

        } else {
            eurAmountLabel.isHidden = true
            eurCurrencyLabel.isHidden = true
        }
    }
}
