//
//  RequestManager.swift
//  StatisticsGNB
//
//  Created by Eduard Preduca on 1/10/19.
//  Copyright © 2019 Eduard Preduca. All rights reserved.
//

import Foundation
import Alamofire

class RequestManager {
    
    // MARK: Properties
    
    /// current instance of this class, singleton instance.
    
    static let shared = RequestManager()
    
    let headers: HTTPHeaders = [
        "Accept": "application/json"
    ]
    
    let baseURL = "http://gnb.dev.airtouchmedia.com/"
    
    var rates = [Rate]()

}

extension RequestManager {
    
    func getRates(onSucces: @escaping([Rate]?) -> Void, onFail: @escaping(Error) -> Void) {
        let url = baseURL + "rates.json"

        Alamofire.request(url, method: .get, headers: headers).responseJSON { (response) in
 
            if response.error == nil {
                let result = response.result.value as! [[String: Any]]
                var allRates = [Rate]()
                for object in result {
                    allRates.append(Rate(json: object))
                }
                
                try! self.savePropertyList(result, url: self.ratesPlistURL)
                onSucces(allRates)
            } else {
                onFail(response.error!)
            }
        }
    }
    
    func getTransactions(onSucces: @escaping([Transaction]?) -> Void, onFail: @escaping(Error) -> Void) {
        let url = baseURL + "transactions.json"
        
        Alamofire.request(url, method: .get, headers: headers).responseJSON { (response) in

            if response.error == nil {
                let result = response.result.value as! [[String: Any]]
                var allTransaction = [Transaction]()
                for object in result {
                    allTransaction.append(Transaction(json: object))
                }
                try! self.savePropertyList(result, url: self.transactionsPlistURL)

                onSucces(allTransaction)
            } else {
                onFail(response.error!)
            }
        }
    }
}

extension RequestManager {
    
    var transactionsPlistURL : URL {
        let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return documentDirectoryURL.appendingPathComponent("transactions.plist")
    }
    
    var ratesPlistURL: URL {
        let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return documentDirectoryURL.appendingPathComponent("rates.plist")
    }
    
    
    func savePropertyList(_ plist: Any, url: URL) throws
    {
        let plistData = try PropertyListSerialization.data(fromPropertyList: plist, format: .xml, options: 0)
        try plistData.write(to: url)
    }
    
    
    func loadTransactions() throws -> [Transaction]
    {
        guard  let data =  try? Data(contentsOf: transactionsPlistURL) else {
            return [Transaction]()
        }
        
        guard let result = try PropertyListSerialization.propertyList(from: data, format: nil) as?  [[String: Any]] else {
            return [Transaction]()
        }
        var allTransaction = [Transaction]()
        for object in result {
            allTransaction.append(Transaction(json: object))
        }
        
        return allTransaction
    }
    
    func loadRates() throws -> [Rate] {
        guard let data = try? Data(contentsOf: ratesPlistURL) else {
            return [Rate]()
        }
        guard let result = try PropertyListSerialization.propertyList(from: data, format: nil) as?  [[String: Any]] else {
            return [Rate]()
        }
        
        var allRates = [Rate]()
        for object in result {
            allRates.append(Rate(json: object))
        }
        
        return allRates
    }
}
