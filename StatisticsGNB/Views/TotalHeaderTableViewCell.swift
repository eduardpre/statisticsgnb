//
//  TotalHeaderTableViewCell.swift
//  StatisticsGNB
//
//  Created by Eduard Preduca on 1/10/19.
//  Copyright © 2019 Eduard Preduca. All rights reserved.
//

import UIKit

class TotalHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var totalAmountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
