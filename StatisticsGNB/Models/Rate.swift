//
//  Rate.swift
//  StatisticsGNB
//
//  Created by Eduard Preduca on 1/10/19.
//  Copyright © 2019 Eduard Preduca. All rights reserved.
//

import Foundation

class Rate {
    var fromCurrency: String!
    var toCurrency: String!
    var rate: Double!
    
    convenience init(json: [String : Any]) {
        self.init()
        self.fromCurrency = json["from"] as! String
        self.toCurrency = json["to"] as! String
        self.rate = Double(json["rate"] as! String)
    }
    
    convenience init(fromCurrency: String, toCurrency: String, rate: Double) {
        self.init()
        self.fromCurrency = fromCurrency
        self.toCurrency = toCurrency
        self.rate = rate
    }
}
