//
//  TransactionsViewController.swift
//  StatisticsGNB
//
//  Created by Eduard Preduca on 1/10/19.
//  Copyright © 2019 Eduard Preduca. All rights reserved.
//

import UIKit

class TransactionsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var transactions = [Transaction]()
    var transactionName = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")

            self.transactions = try! RequestManager.shared.loadTransactions()
            self.transactionName = self.uniqueTransactions(transactions: self.transactions)

            RequestManager.shared.rates = try! RequestManager.shared.loadRates()
        
            self.tableView.reloadData()
        getData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Helper methods
    
    func uniqueTransactions(transactions : [Transaction]) -> [String] {
        var unique = [String]()
        for (_, value) in transactions.enumerated() {
            if !unique.contains(value.sku) {
                unique.append(value.sku)
            }
        }
        return unique
    }
    
    func filterTransactions(transactionName : String) -> [Transaction] {
       let filterTransaction =  transactions.filter { (transaction) -> Bool in
            return transaction.sku == transactionName
        }
        
        return filterTransaction
    }
    
    func getData() {
        RequestManager.shared.getRates(onSucces: { (rates) in
            RequestManager.shared.rates = rates!
        }) { (error) in
            
        }
        
        RequestManager.shared.getTransactions(onSucces: { (transactions) in
            self.transactions = transactions!
            self.transactionName = self.uniqueTransactions(transactions: self.transactions)
            self.tableView.reloadData()
        }) { (error) in
            
        }

    }
}

// MARK: Table view datasource and delegate

extension TransactionsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = transactionName[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        self.performSegue(withIdentifier: "showTransactionDetails", sender: transactionName[indexPath.row])
        
    }
    
    // MARK: Actions
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showTransactionDetails") {
            let controller = segue.destination as? TransactionsDetailsViewController
            let transactions = filterTransactions(transactionName: sender as! String)
            controller?.transactions = transactions
        }
    }
    
}
