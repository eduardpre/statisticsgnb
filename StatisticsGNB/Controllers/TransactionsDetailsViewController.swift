//
//  TransactionsDetailsViewController.swift
//  StatisticsGNB
//
//  Created by Eduard Preduca on 1/10/19.
//  Copyright © 2019 Eduard Preduca. All rights reserved.
//

import UIKit

class TransactionsDetailsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var transactions = [Transaction]()
    var totalTransactions : Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for transaction in transactions {
            totalTransactions += transaction.eurAmout
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

// MARK: Table view datasource and delegate

extension TransactionsDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsTableViewCell", for: indexPath) as! DetailsTableViewCell
        cell.updateWithTransaction(transaction: transactions[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "TotalHeaderTableViewCell") as! TotalHeaderTableViewCell
        headerView.totalAmountLabel.text = "\(totalTransactions) EUR"
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}
