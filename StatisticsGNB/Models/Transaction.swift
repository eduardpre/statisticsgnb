//
//  Transaction.swift
//  StatisticsGNB
//
//  Created by Eduard Preduca on 1/10/19.
//  Copyright © 2019 Eduard Preduca. All rights reserved.
//

import Foundation


class Transaction {
    var sku: String!
    var amount: Double! = 0.00
    var currency: String!
    
    var eurAmout : Double {
        
        let (_, amount) = self.convertCurrency(amount: self.amount, fromCurrency: currency, toCurrency: "EUR")
        return Double(lrint(amount))
        
    }
    
    func convertCurrency(amount: Double, fromCurrency: String, toCurrency: String) -> (Rate, Double) {
        
        var foundAmount: Double = 0.0
        var foundRate : Rate! = nil
        
        let rate = rates.first { (rate) -> Bool in
            return rate.fromCurrency == fromCurrency && rate.toCurrency == toCurrency
        }
        
        if rate == nil {
            
            let rate2 = rates.first { (rate) -> Bool in
                return rate.fromCurrency == fromCurrency && rate.toCurrency != "EUR"
            }
            
            if rate2 != nil {
                
                let newAmount = amount * rate2!.rate
                
                let (rate3, amount3) = self.convertCurrency(amount: newAmount, fromCurrency: rate2!.toCurrency, toCurrency: "EUR")
                
                foundAmount = amount3
                foundRate = rate3
                
            }
            
            return (foundRate, foundAmount)
        
        } else {
            foundAmount = amount * rate!.rate
            foundRate = rate
            return (foundRate, foundAmount)
        }
    }
    
    convenience init(json: [String : Any]) {
        self.init()
        self.sku = json["sku"] as! String
        self.amount = Double(json["amount"] as! String)
        self.currency = json["currency"] as! String
    }
    
    convenience init(sku: String, amount: Double, currency: String) {
        self.init()
        self.sku = sku
        self.amount = amount
        self.currency = currency
    }
    
    var rates : [Rate] {
        return RequestManager.shared.rates
    }
}
